package com.air.patterns.design.structural.abstractfactory;

public abstract class AbstractFactory {

    abstract Color getColor(String color);

    abstract Shape getShape(String shape);
}