package com.air.patterns.design.structural.abstractfactory;

public interface Color {
    void fill();
}