package com.air.patterns.design.structural.abstractfactory;

public interface Shape {
    void draw();
}
