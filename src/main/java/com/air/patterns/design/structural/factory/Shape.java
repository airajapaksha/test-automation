package com.air.patterns.design.structural.factory;

public interface Shape {
    void draw();
}
