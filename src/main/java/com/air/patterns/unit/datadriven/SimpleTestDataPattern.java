package com.air.patterns.unit.datadriven;

public class SimpleTestDataPattern {

    public int addNumbers(int firstNumber, int secondNumber) {

        return firstNumber + secondNumber;

    }
}
