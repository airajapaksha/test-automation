package com.air.patterns.unit.passfail;

public class CodePathPattern {

    public String checkEvenOrOdd(int number) {

        String result = "None";

        if ((number & 1) == 0) {
            result = "Even";
        } else {
            result = "Odd";
        }

        return result;
    }
}
