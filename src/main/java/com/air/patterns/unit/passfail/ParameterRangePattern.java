package com.air.patterns.unit.passfail;

public class ParameterRangePattern {

    public String isAlive(int age) {

        String result = "Not Sure";

        if (0 < age && age <= 120) {
            result = "Yes";
        } else {
            result = "No";
        }

        return result;
    }
}
