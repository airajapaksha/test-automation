package com.air.patterns.unit.passfail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SimpleTestPattern {

    /**
   * This method is used to increment a date.
   * @param date This is the first parameter. Format "yyyy-MM-dd"
   * @param dateCount This is the second parameter
   * @return String This returns incremented date
   */
    public String incrementDate(String date, int dateCount) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(date));
        c.add(Calendar.DATE, dateCount);
        date = sdf.format(c.getTime());

        return date;
    }
}
