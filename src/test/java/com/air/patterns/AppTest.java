package com.air.patterns;

import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;

public class AppTest {

    App app = new App();

    @Test
    public void testEcho() {
        String expected = "Hello";
        String actual = app.echo("Hello");
        assertEquals(actual, expected);
    }

    @BeforeSuite
    public void beforeSuite() {
        System.out.println("in beforeSuite");
    }

    @AfterSuite
    public void afterSuite() {
        System.out.println("in afterSuite");
    }

    @BeforeClass
    public void beforeClass() {
        System.out.println("in beforeClass");
    }

    @AfterClass
    public void afterClass() {
        System.out.println("in afterClass");
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("in beforeMethod");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("in afterMethod");
    }

}
