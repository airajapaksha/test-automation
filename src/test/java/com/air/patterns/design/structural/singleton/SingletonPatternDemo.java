package com.air.patterns.design.structural.singleton;

import static org.testng.Assert.assertEquals;

public class SingletonPatternDemo {
    public static void main(String[] args) {

        //illegal construct
        //Compile Time Error: The constructor SingleObject() is not visible
        //SingleObject object = new SingleObject();

        //Get the only object available
        SingleObject object1 = SingleObject.getInstance();
        SingleObject object2 = SingleObject.getInstance();

        //show the message
        object1.showMessage();
        object2.showMessage();

        assertEquals(object1, object2);
    }
}
