package com.air.patterns.unit.datadriven;

import org.testng.annotations.Test;
import org.testng.reporters.Files;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertEquals;
import static org.testng.FileAssert.fail;

public class DataTransformationTestPatternTest {

    DataTransformationTestPattern dttp = new DataTransformationTestPattern();

    @Test
    public void testStylizer() {

        try {
            String expected = Files.readFile(new File("src/main/data/expected.html"));
            dttp.stylizer();
            String actual = Files.readFile(new File("src/main/data/actual.html"));
            assertEquals(actual, expected);
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }
}
