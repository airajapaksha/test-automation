package com.air.patterns.unit.datadriven;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SimpleTestDataPatternTest {

    SimpleTestDataPattern stdp = new SimpleTestDataPattern();

    @DataProvider(name = "AddNumbers")
    public Object[][] AddNumbersData() {
        return new Object[][]{
                {-1, 3, 2},
                {1, 3, 4},
                {2, 4, 6},
                {9, 9, 18},
                {5, -3, 2},
                {2, 2, 4}
        };
    }

    @Test(dataProvider = "AddNumbers")
    public void testAddNumbers(int firstNumber, int secondNumber, int expected) {

        int actual = stdp.addNumbers(firstNumber, secondNumber);

        assertEquals(actual, expected);
    }
}
