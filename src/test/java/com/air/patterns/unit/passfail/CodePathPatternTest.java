package com.air.patterns.unit.passfail;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CodePathPatternTest {

    CodePathPattern cpp = new CodePathPattern();

    @Test
    public void testCheckEven() {

        String expected = "Even";
        String actual = cpp.checkEvenOrOdd(2);

        assertEquals(actual, expected);

    }
    @Test
    public void testCheckOdd() {

        String expected = "Odd";
        String actual = cpp.checkEvenOrOdd(3);

        assertEquals(actual, expected);

    }
}
