package com.air.patterns.unit.passfail;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ParameterRangePatternTest {

    ParameterRangePattern prp = new ParameterRangePattern();

    @Test
    public void testIsAliveYes() {

        String expected = "Yes";

        for (int age = 1; age <= 120; age++) {
            String actual = prp.isAlive(age);
            assertEquals(actual, expected, "Age = " + age);
        }
    }

    @Test
    public void testIsAliveNo() {

        String expected = "No";

        for (int age = -100; age <= 220; age++) {

            if (age == 1) {
                age = 121;
            }

            String actual = prp.isAlive(age);
            assertEquals(actual, expected, "Age = " + age);
        }
    }
}
