package com.air.patterns.unit.passfail;

import org.testng.annotations.Test;

import java.text.ParseException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class SimpleTestPatternTest {

    SimpleTestPattern stp = new SimpleTestPattern();

    @Test
    public void testIncrementDate() {

        String date = "2015-12-15";
        int dateCount = 5;
        String expected = "2015-12-20";
        String actual = "";

        try {
            actual = stp.incrementDate(date, dateCount);
        } catch (ParseException e) {
            fail(e.toString());
        }

        assertEquals(actual, expected);
    }

    @Test
    public void testIncrementDateNegative() {

        String date = "2015-12-dd";
        int dateCount = 5;
        String expected = "Unparseable date: \"2015-12-dd\"";
        String actual;

        try {
            actual = stp.incrementDate(date, dateCount);
        } catch (ParseException e) {
            actual = e.getMessage();
        }

        assertEquals(actual, expected);
    }
}
